resource "aws_key_pair" "webserver-key" {
  key_name   = "webserver-key"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

