# Internet VPC
resource "aws_vpc" "assignment" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "assignment"
  }
}

# Subnets
resource "aws_subnet" "assignment-public-1" {
  vpc_id                  = aws_vpc.assignment.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"

  tags = {
    Name = "assignment-public-1"
  }
}

resource "aws_subnet" "assignment-public-2" {
  vpc_id                  = aws_vpc.assignment.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1b"

  tags = {
    Name = "assignment-public-2"
  }
}

resource "aws_subnet" "assignment-public-3" {
  vpc_id                  = aws_vpc.assignment.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1c"

  tags = {
    Name = "assignment-public-3"
  }
}

# Internet GW
resource "aws_internet_gateway" "assignment-gw" {
  vpc_id = aws_vpc.assignment.id

  tags = {
    Name = "assignment"
  }
}

# route tables
resource "aws_route_table" "assignment-public" {
  vpc_id = aws_vpc.assignment.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.assignment-gw.id
  }

  tags = {
    Name = "assignment-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "assignment-public-1-a" {
  subnet_id      = aws_subnet.assignment-public-1.id
  route_table_id = aws_route_table.assignment-public.id
}

resource "aws_route_table_association" "assignment-public-2-a" {
  subnet_id      = aws_subnet.assignment-public-2.id
  route_table_id = aws_route_table.assignment-public.id
}

resource "aws_route_table_association" "assignment-public-3-a" {
  subnet_id      = aws_subnet.assignment-public-3.id
  route_table_id = aws_route_table.assignment-public.id
}

