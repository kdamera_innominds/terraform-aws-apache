variable "AWS_REGION" {
  default = "us-east-1"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "SCRIPT" {
  default = "mykey.pub"
}

variable "START" {
   default = "start-apache.sh"
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}

variable "http_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 80
}

variable "ssh_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 22
}

variable "AMI" {
  default = "ami-0b6bc09813204a5a1"
}
