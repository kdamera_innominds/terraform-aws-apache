
resource "aws_instance" "ami-webserver" {
  tags = {
        Name = "Apache-webServer"
  }
  ami           = var.AMI
  instance_type = "t2.micro"
  associate_public_ip_address = "true"

  # the VPC subnet
  subnet_id = aws_subnet.assignment-public-1.id

  # the security group
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  # the public SSH key
  key_name = aws_key_pair.webserver-key.key_name
  user_data = file(var.START)
}